<?php namespace Cutesy\Location\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderCreateCutesyLocationLocations extends Migration
{
    public function up()
    {
        Schema::create('cutesy_location_locations', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('city_id')->unsigned()->nullable();
            $table->string('street_name')->nullable();
            $table->integer('house_number')->nullable();
            $table->integer('building_number')->nullable();
            $table->decimal('longitude', 9, 6)->nullable();
            $table->decimal('latitude', 9, 6)->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('cutesy_location_locations');
    }
}