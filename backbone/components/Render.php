<?php namespace Cutesy\Backbone\Components;

use AjaxException;
use Cms\Classes\Page;
use Cms\Classes\Theme;
use Cms\Classes\ComponentBase;

class Render extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Render Component',
            'description' => ''
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function init()
    {
        $this->sessionComponent = $this->addComponent(
            'Cutesy\User\Components\Session',
            'sessionComponent',
            ['deferredBinding' => false]
        );

        $this->accountComponent = $this->addComponent(
            'Cutesy\User\Components\Account',
            'accountComponent',
            ['deferredBinding' => false]
        );

        $this->profilesComponent = $this->addComponent(
            'Cutesy\Community\Components\Profile',
            'profileComponent',
            ['deferredBinding' => false]
        );

        $this->profilesComponent = $this->addComponent(
            'Cutesy\Community\Components\Community',
            'communityComponent',
            ['deferredBinding' => false]
        );
    }

    public function onRenderPage()
    {
        $theme          = Theme::getActiveTheme();
        $pageFile       = post('pageFile')      ? post('pageFile')      : null;
        $parameters     = post('parameters')    ? post('parameters')    : [];

        if (!is_array($parameters))
            $parameters = [$parameters];

        try {
            if ($page = Page::load($theme, $pageFile)) {

                $pageContent = $this->render($pageFile, $parameters);

                return [
                    'pageTitle' => $page->title,
                    'pageContent' => $pageContent
                ];
            }
        }
        catch (Exception $e) {
            throw new AjaxException($e);
        }
    }

    public function onRenderPartial()
    {
        $component = post('component') ? post('component') : null;
        $partialFile = post('partialFile') ? post('partialFile') : null;
        $partialPath = $component ? "{$component}::{$partialFile}" : $partialFile;
        $parameters = post('parameters') ? post('parameters') : [];

        if (!is_array($parameters))
            $parameters = [$parameters];

        try {
            if ($partialContent = $this->renderPartial($partialPath, $parameters)) {

                return [
                    'partialContent' => $partialContent
                ];
            }
        }
        catch (Exception $e) {
            throw new AjaxException($e);
        }
    }
}