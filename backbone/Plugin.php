<?php namespace Cutesy\Backbone;

use Redirect;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Cutesy\Backbone\Components\Render' => 'renderComponent',
        ];
    }

    public function registerMarkupTags()
    {
        return [
            'functions' => [
                'redirect' => function($target) { return Redirect::to($target); }
            ]
        ];
    }

    public function registerSettings()
    {
    }
}
