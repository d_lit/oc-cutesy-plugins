<?php namespace Cutesy\Community\Components;

use Auth;
use Event;
use Input;
use Response;
use Redirect;
use ApplicationException;
use Cutesy\Community\Models\Profile as ProfileModel;
use Cutesy\Community\Models\Service as ServiceModel;
use Cms\Classes\ComponentBase;

class Profile extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Profile Component',
            'description' => ''
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function loadProfile($alias)
    {
        $profile = ProfileModel::getByAlias($alias)->isActive()
            ->with(['user', 'photos', 'profession', 'services'])
            ->first();

        return $profile;
    }

    public function loadServices()
    {
        return ServiceModel::all();
    }

    public function onCreate()
    {
        try {
            $data = post();
            $user = $this->user();

            if (Event::fire('cutesy.profile.create', [$this, $user, $data])) {

                return ['message' => 'Профиль успешно создан!'];

            }
        }
        catch (Exception $e) {
            throw new ApplicationException($e->getMessage());
        }
    }

    public function onComplete()
    {
        $data = post();

        $user = $this->user();
        $profile = $user->profile;

        if ($profile->role == $profile::ROLE_BEAUTY || $user->hasUsedInvite()) {
            try {
                if (array_merge(
                        Event::fire('cutesy.profile.update',   [$this, $user, $profile, $data]),
                        Event::fire('cutesy.profile.complete', [$this, $user, $profile, $data])
                    )) {

                    return [
                        'message' => 'Профиль успешно активирован!'
                    ];

                }
            }
            catch (Exception $e) {
                throw new ApplicationException($e->getMessage());
            }
        }
        else {
            try {
                if (array_merge(
                        Event::fire('cutesy.profile.update', [$this, $user, $profile, $data]),
                        Event::fire('cutesy.request.create', [$this, $user, $profile, $data])
                    )) {

                    return [
                        'message' => '
                            <i class="uk-icon uk-icon-send"></i>&nbsp;&nbsp;
                            Ваш запрос успешно отправлен, дождитесь ответа Марии!
                        '
                    ];

                }
            }
            catch (Exception $e) {
                throw new ApplicationException($e->getMessage());
            }
        }
    }

    public function onUpdate()
    {
        $data = post();

        $user = $this->user();
        $profile = $user->profile;

        try {
            if (Event::fire('cutesy.profile.update', [$this, $user, $profile, $data])) {

                return [
                    'message' => '
                        <i class="uk-icon uk-icon-magic"></i>&nbsp;&nbsp;
                        Изменения успешно сохранены.
                    '
                ];

            }
        }
        catch (Exception $e) {
            throw new ApplicationException($e->getMessage());
        }
    }

    public function onDeactivate()
    {
        //
    }

    public function onReactivate()
    {
        //
    }

    public function user()
    {
        if (!Auth::check()) {
            return null;
        }

        return Auth::getUser();
    }
}