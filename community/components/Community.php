<?php namespace Cutesy\Community\Components;

use Auth;
use Event;
use Input;
use Response;
use Redirect;
use ApplicationException;
use Cutesy\Community\Models\Profile as ProfileModel;
use Cutesy\Community\Models\Service as ServiceModel;
use Cms\Classes\ComponentBase;

class Community extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Community Component',
            'description' => ''
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function loadServices()
    {
        return ServiceModel::all();
    }

    public function onSearch()
    {
        $data = post();

        $filter = $data ? $this->prepareFilterArray($data) : [];

        $query = ProfileModel::isPro()->isActive()->with(['user', 'profession', 'services']);

        if (!empty(array_filter($filter))) {
            $query->applySearchFilter($filter);
        }

        $totalCount = $query->count();

        $query->orderBy('id', 'desc');

        if (!is_null($data['offset'])) {
            $query->skip($data['offset']);
        }

        $profiles = $query->take(20)->get();

        if ($profiles->isEmpty()) {
            return [
                'title' => $this->controller->renderPartial('community/results_fail.htm'),
            ];
        }
        else  {
            if(!$data['offset']) {
                // first render
                return [
                    'title'   => $this->controller->renderPartial('community/results_title.htm',   ['totalCount' => $totalCount]),
                    'list'    => $this->controller->renderPartial('community/results_list.htm',    ['profiles'   => $profiles]),
                ];
            } else {
                return [
                    'list'    => $this->controller->renderPartial('community/results_list.htm',    ['profiles'   => $profiles]),
                ];
            }
        }
    }

    public function prepareFilterArray($data)
    {
        $services  = array_key_exists('services',  $data) && !empty($data['services'])  ? $data['services']  : null;
        $min_price = array_key_exists('min_price', $data) && !empty($data['min_price']) ? $data['min_price'] : null;
        $max_price = array_key_exists('max_price', $data) && !empty($data['max_price']) ? $data['max_price'] : null;

        return [
            'services'  => $services,
            'min_price' => $min_price,
            'max_price' => $max_price
        ];
    }

    public static function formPlural ($number, $before, $after)
    {
        $cases = array(2,0,1,1,1,2);
        echo $before[($number%100>4 && $number%100<20)? 2: $cases[min($number%10, 5)]].' '.$number.' '.$after[($number%100>4 && $number%100<20)? 2: $cases[min($number%10, 5)]];
    }
}