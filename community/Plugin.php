<?php namespace Cutesy\Community;

use App;
use Event;
use Backend;
use System\Classes\PluginBase;
use Illuminate\Foundation\AliasLoader;

class Plugin extends PluginBase
{
    /**
    * @var array Plugin dependencies
    */
    public $require = ['Cutesy.User'];

    public function registerComponents()
    {
        return [
            'Cutesy\Community\Components\Profile'  => 'profileComponent',
            'Cutesy\Community\Components\Community' => 'communityComponent',
        ];
    }

    public function registerSettings()
    {
    }

    public function register()
    {
    }

    public function registerMarkupTags()
    {
        return [
            'functions' => [
                'form_select_profession' => ['Cutesy\Community\Models\Profession', 'formSelect'],
                'form_plural' => ['Cutesy\Community\Components\Community', 'formPlural']
            ]
        ];
    }

    public function boot()
    {
        App::singleton('InviteEventHandler', function() {
            return new \Cutesy\Community\Classes\InviteEventHandler;
        });

        App::singleton('RequestEventHandler', function() {
            return new \Cutesy\Community\Classes\RequestEventHandler;
        });

        App::singleton('ProfileEventHandler', function() {
            return new \Cutesy\Community\Classes\ProfileEventHandler;
        });

        Event::subscribe('InviteEventHandler');
        Event::subscribe('RequestEventHandler');
        Event::subscribe('ProfileEventHandler');
    }
}
