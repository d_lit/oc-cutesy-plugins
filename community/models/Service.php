<?php namespace Cutesy\Community\Models;

use Lang;
use Model;

/**
 * Service Model
 */
class Service extends Model
{
    public $table = 'cutesy_community_services';
    public $timestamps = false;

    protected $guarded = ['*'];
    protected $fillable = [];

    public $belongsToMany = [
        'profiles' => [
            'Cutesy\Community\Models\Profile',
            'table' => 'cutesy_community_profiles_services',
            'pivot' => ['min_price', 'max_price']
        ]
    ];
}