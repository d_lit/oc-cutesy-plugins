<?php namespace Cutesy\Community\Models;

use Lang;
use Model;

/**
 * Request Model
 */
class Request extends Model
{
    const STATUS_FRESH = 'fresh';
    const STATUS_APPROVED = 'approved';
    const STATUS_REJECTED  = 'rejected';

    public $table = 'cutesy_community_requests';

    public $belongsTo = [
        'user' => ['Cutesy\User\Models\User'],
        'profile' => ['Cutesy\Community\Models\Profile'],
    ];

    protected $dates = ['approved_at', 'rejected_at'];

    protected $fillable = ['status'];

    protected $appends = ['current_status'];

    public function getStatusOptions($keyValue = null)
    {
        return [
            self::STATUS_FRESH => Lang::get('cutesy.community::lang.requests.fields.status.' . self::STATUS_FRESH),
            self::STATUS_APPROVED => Lang::get('cutesy.community::lang.requests.fields.status.' . self::STATUS_APPROVED),
            self::STATUS_REJECTED => Lang::get('cutesy.community::lang.requests.fields.status.' . self::STATUS_REJECTED),
        ];
    }

    public function getCurrentStatusAttribute()
    {
        if ($this->status) {
            return $this->getStatusOptions()[$this->status];
        }
    }

    public function scopeIsFresh($query)
    {
        return $query->where('status', self::STATUS_FRESH);
    }

    public function scopeIsApproved($query)
    {
        return $query->where('status', self::STATUS_APPROVED);
    }

    public function scopeIsRejected($query)
    {
        return $query->where('status', self::STATUS_REJECTED);
    }
}