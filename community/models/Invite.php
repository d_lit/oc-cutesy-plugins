<?php namespace Cutesy\Community\Models;

use Lang;
use Mail;
use Model;
use Event;
use Exception;

/**
 * Invitation Model
 */
class Invite extends Model
{
    use \October\Rain\Database\Traits\Purgeable;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'cutesy_community_invites';

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'invitee'   => ['Cutesy\User\Models\User', 'key' => 'invitee_id']
    ];
    public $morphTo = [
        'inviter'   => []
    ];

    /**
     * @var array The attributes that should be mutated to dates.
     */
    protected $dates = ['valid_until'];

    /**
     * @var array The attributes that should be hidden for arrays.
     */
    protected $hidden = ['invitation_code'];

    /**
     * @var array The attributes that aren't mass assignable.
     */
    protected $guarded = ['invitation_code'];

    /**
     * @var array The attributes that are mass assignable.
     */
    protected $fillable = ['is_sent'];

    /**
     * @var array Purge attributes from data set.
     */
    protected $purgeable = ['send_invite'];

    public function afterCreate()
    {
        $send_invite = $this->getOriginalPurgeValue('send_invite');

        if ($send_invite) {
            $this->onSendInvitation();

            $this->is_sent = true;
            $this->sent_at = $this->freshTimestamp();
            $this->save();
        }
    }

    public function onSendInvitation()
    {
        $code = implode('!', [$this->id, $this->getInvitationCode()]);

        $link = url() . '/join/';

        $data = ['link' => $link, 'code' => $code];

        Mail::sendTo($this->email, 'cutesy.community::mail.invitation', $data);
    }

    /**
     * Get an invite code for the given user.
     * @return string
     */
    public function getInvitationCode()
    {
        $this->invitation_code = $inviteCode = str_random(10);
        $this->save();

        return $inviteCode;
    }

    /**
     * @param string $inviteCode
     * @return bool
     */
    public function attemptInvitationCode($invitationCode)
    {
        if ($this->is_used)
            throw new Exception('Приглашение уже использовано. Займитесь чем-нибудь серьезным.');

        if ($invitationCode == $this->invitation_code) {
            $this->invitation_code = null;
            $this->is_used = true;
            $this->used_at = $this->freshTimestamp();
            $this->save();
            return true;
        }

        return false;
    }

    public static function findByEmail($email)
    {
        if (!$email) return;
        return self::where('email', $email)->first();
    }
}