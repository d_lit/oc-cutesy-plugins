<?php namespace Cutesy\Community\Models;

use Form;
use Model;

/**
 * Profession Model
 */
class Profession extends Model
{
    public $table = 'cutesy_community_professions';
    public $timestamps = false;

    protected $guarded = ['*'];
    protected $fillable = [];

    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * @var array Cache for nameList() method
     */
    protected static $nameList = null;

    public static function getNameList()
    {
        if (self::$nameList) {
            return self::$nameList;
        }
        return self::$nameList = self::lists('name', 'id');
    }

    public static function formSelect($name, $selectedValue = null, $options = [])
    {
        return Form::select($name, self::getNameList(), $selectedValue, $options);
    }
}