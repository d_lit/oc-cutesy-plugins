<?php namespace Cutesy\Community\Models;

use Lang;
use Model;

/**
 * Profile Model
 */
class Profile extends Model
{
    use \October\Rain\Database\Traits\SoftDelete;

    const ROLE_BEAUTY = 'beauty';
    const ROLE_PRO = 'pro';

    const STATUS_ACTIVE = 'active';
    const STATUS_HIDDEN = 'hidden';
    const STATUS_FRESH = 'fresh';
    const STATUS_APPROVAL = 'approval';
    const STATUS_HEALING  = 'healing';

    public $table = 'cutesy_community_profiles';

    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'user'              => ['Cutesy\User\Models\User'],
        'profession'        => ['Cutesy\Community\Models\Profession'],
    ];
    public $belongsToMany = [
        'services'          => [
            'Cutesy\Community\Models\Service',
            'table' => 'cutesy_community_profiles_services',
            'pivot' => ['min_price', 'max_price']
        ]
    ];
    public $attachOne = [
        'portrait'          => ['System\Models\File'],
    ];
    public $attachMany = [
        'photos'            => ['System\Models\File'],
    ];

    protected $fillable = ['role', 'status', 'details'];

    /**
     * Values are encoded as JSON
     * @var array
     */
    protected $jsonable = ['details'];

    protected $appends = ['current_status', 'current_role'];

    public function getStatusOptions($keyValue = null)
    {
        return [
            self::STATUS_ACTIVE => Lang::get('cutesy.community::lang.profiles.fields.status.' . self::STATUS_ACTIVE),
            self::STATUS_HIDDEN => Lang::get('cutesy.community::lang.profiles.fields.status.' . self::STATUS_HIDDEN),
            self::STATUS_FRESH => Lang::get('cutesy.community::lang.profiles.fields.status.' . self::STATUS_FRESH),
            self::STATUS_APPROVAL => Lang::get('cutesy.community::lang.profiles.fields.status.' . self::STATUS_APPROVAL),
            self::STATUS_HEALING => Lang::get('cutesy.community::lang.profiles.fields.status.' . self::STATUS_HEALING),
        ];
    }

    public function getRoleOptions($keyValue = null)
    {
        return [
            self::ROLE_BEAUTY => Lang::get('cutesy.community::lang.profiles.fields.role.' . self::ROLE_BEAUTY),
            self::ROLE_PRO => Lang::get('cutesy.community::lang.profiles.fields.role.' . self::ROLE_PRO),
        ];
    }

    public function scopeFilterServices($query, $services)
    {
        return $query->whereHas('services', function($q) use ($services) {
            $q->whereIn('id', $services);
        });
    }

    public function scopeApplySearchFilter($query, $options)
    {
        return $query->whereHas('services', function($query) use ($options) {
            $query = is_null($options['services'])  ? $query : $query->whereIn('service_id', array_flatten($options['services']));
            $query = is_null($options['min_price']) ? $query : $query->where('max_price', '>=', $options['min_price']);
            $query = is_null($options['max_price']) ? $query : $query->where('min_price', '<=', $options['max_price']);
        });
    }

    public function scopeSearchByLastName($query, $surname)
    {
        return $query->where('surname', 'LIKE', '%'.$surname.'%');
    }

    public function scopeGetByUid($query, $uid)
    {
        return $this->whereHas('user', function($query) use ($uid) {
            $query->where('uid', $uid);
        });
    }

    public function scopeGetByAlias($query, $alias)
    {
        return $this->whereHas('user', function($query) use ($alias) {
            $query->where('alias', $alias);
        });
    }

    public function getMinPriceValue()
    {
        return DB::table('cutesy_community_services')->min('min_price');
    }

    public function getMaxPriceValue()
    {
        return DB::table('cutesy_community_services')->max('max_price');
    }

    public function getLikesAttribute()
    {
        return $this->likes()->count();
    }

    public function getCurrentStatusAttribute()
    {
        if ($this->status) {
            return $this->getStatusOptions()[$this->status];
        }
    }

    public function getCurrentRoleAttribute()
    {
        if ($this->role) {
            return $this->getRoleOptions()[$this->role];
        }
    }

    public function scopeIsBeauty($query)
    {
        return $query->where('role', self::ROLE_BEAUTY);
    }

    public function scopeIsPro($query)
    {
        return $query->where('role', self::ROLE_PRO);
    }

    public function scopeIsActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    public function scopeIsHidden($query)
    {
        return $query->where('status', self::STATUS_HIDDEN);
    }

    public function scopeIsFresh($query)
    {
        return $query->where('status', self::STATUS_FRESH);
    }

    public function scopeIsApproval($query)
    {
        return $query->where('status', self::STATUS_APPROVAL);
    }

    public function scopeIsHealing($query)
    {
        return $query->where('status', self::STATUS_HEALING);
    }
}