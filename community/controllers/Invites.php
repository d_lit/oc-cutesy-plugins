<?php namespace Cutesy\Community\Controllers;

use Log;
use Mail;
use Event;
use Redirect;
use BackendMenu;
use Backend\Classes\Controller;
use Cutesy\Community\Models\User;

/**
 * Invites Back-end Controller
 */
class Invites extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Cutesy.Community', 'community', 'invites');
    }

    public function update($recordId, $context = null)
    {
        $this->bodyClass = 'compact-container';

        return $this->asExtension('FormController')->update($recordId, $context);
    }


    public function update_onSendInvite($recordId = null)
    {
        $model = $this->formFindModelObject($recordId);

        $model->onSendInvitation();

        $model->is_sent = true;
        $model->sent_at = $model->freshTimestamp();
        $model->save();

        return Redirect::refresh();
    }

    public function formBeforeCreate($model)
    {
        $model->inviter_id = $this->user->id;
        $model->inviter_type = 'Backend\Models\User';
    }
}