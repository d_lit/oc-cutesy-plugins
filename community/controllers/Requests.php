<?php namespace Cutesy\Community\Controllers;

use Mail;
use Event;
use Redirect;
use BackendMenu;
use Backend\Classes\Controller;

/**
 * Requests Back-end Controller
 */
class Requests extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    public $bodyClass = 'compact-container';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Cutesy.Community', 'community', 'requests');
    }

    public function index()
    {
        $this->bodyClass = 'compact-container';
        $this->asExtension('ListController')->index();
    }

    public function update_onRequestApprove($recordId = null)
    {
        $model = $this->formFindModelObject($recordId);

        if (Event::fire('cutesy.request.approve', [$this, $model])) {
            //Mail::sendTo($model->user->email, 'cutesy.community::mail.request_approved', []);

            return Redirect::refresh();
        }
    }

    public function update_onRequestReject($recordId = null)
    {
        $model = $this->formFindModelObject($recordId);

        if (Event::fire('cutesy.request.reject', [$this, $model])) {
            //Mail::sendTo($request->getEmailByUser()->address, 'cutesy.community::mail.request_rejected', []);

            return Redirect::refresh();
        }
    }
}