<?php namespace Cutesy\Community\Controllers;

use Flash;
use BackendMenu;
use Backend\Classes\Controller;
use Cutesy\Community\Models\Profile as ProfileModel;

/**
 * Profiles Back-end Controller
 */
class Profiles extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public $bodyClass = 'compact-container';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Cutesy.Community', 'community', 'profiles');
    }

    public function index_onDelete()
    {
        if (($checkedIds = post('checkedProfiles')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $profileId) {
                if (!$profile = ProfileModel::find($profileId))
                    continue;

                $profile->delete();
            }

            Flash::success('Выбранные профили были успешно удалены.');
        }

        return $this->listRefresh();
    }

    public function formExtendFields($form)
    {
        if ($form->model->role == ProfileModel::ROLE_BEAUTY) {
            $form->addFields([
                'details[cosmetics]' => [
                    'label' => 'cutesy.community::lang.profiles.fields.beauty.cosmetics',
                    'type' => 'textarea',
                    'size' => 'small',
                ],
                'photos' => [
                    'label' => 'cutesy.community::lang.profiles.fields.beauty.images',
                    'type' => 'fileupload',
                    'mode' => 'image',
                ]
            ]);
        }
        elseif ($form->model->role == ProfileModel::ROLE_PRO) {
            $form->addFields([
                'details[cosmetics]' => [
                    'label' => 'cutesy.community::lang.profiles.fields.pro.cosmetics',
                    'type' => 'textarea',
                    'size' => 'small',
                ],
                'photos' => [
                    'label' => 'cutesy.community::lang.profiles.fields.pro.portfolio',
                    'type' => 'fileupload',
                    'mode' => 'image',
                ]
            ]);
        }
    }
}