<?php namespace Cutesy\Community\Classes;

use Cutesy\Community\Models\Profile;
use Cutesy\Community\Models\Profession;

class ProfileEventHandler
{
    public function onProfileCreate($component, $user, $data)
    {
        if ($user->profile()->save(new Profile(['role' => $data['role'], 'status' => Profile::STATUS_FRESH]))) {
            return true;
        }
    }

    public function onProfileComplete($component, $user, $profile, $data)
    {
        $profile->update(['status' => $profile::STATUS_ACTIVE]);

        return true;
    }

    public function onProfileUpdate($component, $user, $profile, $data)
    {
        if (array_key_exists('details', $data)) {
            $profile->update(['details' => $data['details']]);
        }

        if (array_key_exists('profession', $data)) {
            $profile->profession()->associate(Profession::find($data['profession']));
            $profile->save();
        }

        if (array_key_exists('services', $data)) {
            $services = array_filter($data['services'], function($value) {
                return ($value != null);
            });

            if (array_key_exists('pricerange', $data)) {
                $pricerange = $data['pricerange'];
            }
            else {
                $pricerange = [];
            }

            $mergedArray = array_intersect_key($pricerange, $services);

            $profile->services()->sync($mergedArray);

        } else {
            $profile->services()->sync([]);
        }

        return true;
    }

    public function onRequestCreate($component, $user, $profile)
    {
        $profile->update(['status' => $profile::STATUS_APPROVAL]);

        return true;
    }

    public function onRequestApprove($component, $request)
    {
        $profile = $request->profile;

        $profile->update(['status' => $profile::STATUS_ACTIVE]);

        return true;
    }

    public function onRequestReject($component, $request)
    {
        $profile = $request->profile;

        $profile->update(['status' => $profile::STATUS_HEALING]);

        return true;
    }

    public function subscribe($events)
    {
        $events->listen('cutesy.profile.create',    'ProfileEventHandler@onProfileCreate');
        $events->listen('cutesy.profile.complete',  'ProfileEventHandler@onProfileComplete');
        $events->listen('cutesy.profile.update',    'ProfileEventHandler@onProfileUpdate');

        $events->listen('cutesy.user.register',     'ProfileEventHandler@onProfileCreate');

        $events->listen('cutesy.request.create',    'ProfileEventHandler@onRequestCreate');
        $events->listen('cutesy.request.approve',   'ProfileEventHandler@onRequestApprove');
        $events->listen('cutesy.request.reject',    'ProfileEventHandler@onRequestReject');
    }
}