<?php namespace Cutesy\Community\Classes;

use Cutesy\Community\Models\Invite as InviteModel;

class InviteEventHandler
{
    public function onInviteApply($component, $user, $invite)
    {
        $invite->invitee_id = $user->id;
        $invite->save();

        return true;
    }

    public function onCheckInvitationExists($component, $user, $data)
    {
        if ($invite = InviteModel::findByEmail($user->email)) {
            $invite->invitee_id = $user->id;
            $invite->invitation_code = null;
            $invite->is_used = true;
            $invite->used_at = $invite->freshTimestamp();
            $invite->save();
        }

        return true;
    }

    public function subscribe($events)
    {
        $events->listen('cutesy.invite.apply',  'InviteEventHandler@onInviteApply');

        $events->listen('cutesy.user.register', 'InviteEventHandler@onCheckInvitationExists');
    }
}