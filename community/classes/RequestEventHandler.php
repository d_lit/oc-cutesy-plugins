<?php namespace Cutesy\Community\Classes;

use Carbon\Carbon;
use Cutesy\Community\Models\Request;

class RequestEventHandler
{
    public function onRequestCreate($component, $user, $profile, $data)
    {
        if ($request = Request::create(['status' => REQUEST::STATUS_FRESH])) {

            $request->user()->associate($user)->save();
            $request->profile()->associate($user->profile)->save();

            return true;
        }
    }

    public function onRequestApprove($controller, $request)
    {
        if ($request->update(['approved_at' => Carbon::now(), 'status' => $request::STATUS_APPROVED])) {
            return true;
        }
    }

    public function onRequestReject($controller, $request)
    {
        if ($request->update(['rejected_at' => Carbon::now(), 'status' => $request::STATUS_REJECTED])) {
            return true;
        }
    }

    public function subscribe($events)
    {
        $events->listen('cutesy.request.create',    'RequestEventHandler@onRequestCreate');
        $events->listen('cutesy.request.approve',   'RequestEventHandler@onRequestApprove');
        $events->listen('cutesy.request.reject',    'RequestEventHandler@onRequestReject');
    }
}