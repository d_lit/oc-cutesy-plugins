<?php namespace Cutesy\Community\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderCreateCutesyCommunityProfilesServices extends Migration
{

    public function up()
    {Schema::create('cutesy_community_profiles_services', function($table)
        {
            $table->engine = 'InnoDB';
            //$table->increments('id');
            $table->integer('profile_id')->unsigned();
            $table->integer('service_id')->unsigned();
            $table->integer('min_price')->unsigned()->nullable();
            $table->integer('max_price')->unsigned()->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('cutesy_community_profiles_services');
    }

}
