<?php namespace Cutesy\Community\Updates;

use Faker;
use Seeder;
use Storage;
use Carbon\Carbon;
use System\Models\File;
use Cutesy\User\Models\User;
use Cutesy\Community\Models\Profile;
use Cutesy\Community\Models\Profession;
use Cutesy\Community\Models\Service;

class SeedProfilesTable extends Seeder
{
    public function run()
    {
        $faker = $this->faker = Faker\Factory::create('ru_RU');
        $portraitsTempDir = 'media/temp/portraits';
        $photosTempDir    = 'media/temp/photos';

        for($i = 1; $i <= User::count(); $i++)
        {
            if ($i % 2 != 0) {

                $profile = Profile::create([
                    'user_id'       => $i,
                    'role'          => Profile::ROLE_PRO,
                    'profession_id' => $faker->numberBetween(1, Profession::count()),
                    'details'       => [
                        'experience'    => $faker->sentence($faker->numberBetween(40, 60)),
                        'tools'         => $faker->sentence($faker->numberBetween(40, 60)),
                        'cosmetics'     => $faker->sentence($faker->numberBetween(40, 60)),
                    ],
                    'status'        => Profile::STATUS_ACTIVE,
                ]);

                $this->attachRandomImage($profile, 'portrait', $portraitsTempDir);

                for ($imageIndex = 1; $imageIndex <= $faker->numberBetween(4,8); $imageIndex++) {
                    $this->attachRandomImage($profile, 'photos', $photosTempDir);
                }

                $services = Service::whereIn('id', $faker->randomElements(Service::lists('id'), $faker->numberBetween(1, Service::count())))->get();

                foreach ($services as $service) {
                    $profile->services()->save($service, [
                        'min_price' => $faker->numberBetween(500, 1500),
                        'max_price' => $faker->numberBetween(1500, 3000)
                    ]);
                }
            }
            else {

                $profile = Profile::create([
                    'user_id'       => $i,
                    'role'          => Profile::ROLE_BEAUTY,
                    'profession_id' => $faker->numberBetween(1, 3),
                    'details'       => [
                        'like'          => $faker->sentence($faker->numberBetween(40, 60)),
                        'dislike'       => $faker->sentence($faker->numberBetween(40, 60)),
                        'cosmetics'     => $faker->sentence($faker->numberBetween(40, 60)),
                    ],
                    'status'        => Profile::STATUS_ACTIVE,
                ]);

                $this->attachRandomImage($profile, 'portrait', $portraitsTempDir);

                for ($imageIndex = 1; $imageIndex <= $faker->numberBetween(4,8); $imageIndex++) {
                    $this->attachRandomImage($profile, 'photos', $photosTempDir);
                }
            }
        }
    }

    protected function attachRandomImage($model, $field, $directory)
    {
        $faker = $this->faker;
        $tempMediaImages = Storage::disk('local')->files($directory);

        $path = storage_path('app/' . $tempMediaImages[
            $faker->numberBetween(0, count($tempMediaImages) - 1)
        ]);

        $attach = new File;
        $attach->title = $faker->sentence($faker->numberBetween(3,5));
        $attach->description = $faker->sentence($faker->numberBetween(10,15));
        $attach->fromFile($path)->save();

        $model->$field()->add($attach);
    }
}