<?php namespace Cutesy\Community\Updates;

use Faker;
use Cutesy\Community\Models\Service;
use October\Rain\Database\Updates\Seeder;

class SeedServicesTable extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();

        $service = Service::create(['code' => 'haircut', 'name' => 'Стрижка', 'icon_class' => 'cutesy-icon-scissors']);
        $service = Service::create(['code' => 'makeup' , 'name' => 'Макияж' , 'icon_class' => 'cutesy-icon-mascara']);
        $service = Service::create(['code' => 'styling', 'name' => 'Укладка', 'icon_class' => 'cutesy-icon-hair']);
        $service = Service::create(['code' => 'on-call', 'name' => 'Выезд на дом', 'icon_class' => 'cutesy-icon-taxi']);
    }
}