<?php namespace Cutesy\Community\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderCreateCutesyCommunityProfessions extends Migration
{

    public function up()
    {
        Schema::create('cutesy_community_professions', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('code')->unique();
            $table->string('icon_path')->nullable();
            $table->text('description')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('cutesy_community_professions');
    }

}
