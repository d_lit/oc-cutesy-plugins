<?php namespace Cutesy\Community\Updates;

use Faker;
use Cutesy\Community\Models\Profession;
use October\Rain\Database\Updates\Seeder;

class SeedProfessionsTable extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();

        $profession = Profession::create(['code' => 'makeup-master', 'name' => 'Визажист']);
        $profession = Profession::create(['code' => 'hairdresser'  , 'name' => 'Мастер'  ]);
        $profession = Profession::create(['code' => 'stylist'      , 'name' => 'Стилист' ]);
    }
}