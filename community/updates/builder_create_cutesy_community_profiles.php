<?php namespace Cutesy\Community\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderCreateCutesyCommunityProfiles extends Migration
{

    public function up()
    {
        Schema::create('cutesy_community_profiles', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('profession_id')->unsigned();
            $table->string('role')->nullable()->index();
            $table->string('status')->nullable()->index();
            $table->text('details')->nullable();
            $table->integer('completness')->default(0);
            $table->integer('views_count')->default(0);
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('cutesy_community_profiles');
    }

}
