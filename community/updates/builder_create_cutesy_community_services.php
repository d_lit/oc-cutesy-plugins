<?php namespace Cutesy\Community\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderCreateCutesyCommunityServices extends Migration
{

    public function up()
    {
        Schema::create('cutesy_community_services', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('code')->unique();
            $table->string('icon_path')->nullable();
            $table->string('icon_class')->nullable();
            $table->text('description')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('cutesy_community_services');
    }

}
