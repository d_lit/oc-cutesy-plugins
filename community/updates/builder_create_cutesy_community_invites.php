<?php namespace Cutesy\Community\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderCreateCutesyCommunityInvites extends Migration
{

    public function up()
    {
        Schema::create('cutesy_community_invites', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->morphs('inviter');
            $table->integer('invitee_id')->nullable();
            $table->string('email')->nullable();
            $table->string('invitation_code')->nullable();
            $table->boolean('is_sent')->default(false);
            $table->boolean('is_used')->default(false);
            $table->timestamp('sent_at')->nullable();
            $table->timestamp('used_at')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('cutesy_community_invites');
    }

}
