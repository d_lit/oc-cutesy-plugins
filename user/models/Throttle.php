<?php namespace Cutesy\User\Models;

use October\Rain\Auth\Models\Throttle as ThrottleBase;

class Throttle extends ThrottleBase
{
    /**
     * @var string The database table used by the model.
     */
    protected $table = 'cutesy_user_throttle';

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'user' => ['Cutesy\User\Models\User']
    ];
}
