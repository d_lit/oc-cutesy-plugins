<?php namespace Cutesy\User\Models;

use Model;

/**
 * Phone Model
 */
class Phone extends Model
{
    use \October\Rain\Database\Traits\SoftDelete;

    public $table = 'cutesy_user_phones';
    public $rules = [
        'number' => 'required|between:6,255|unique:cutesy_user_phones',
    ];

    protected $guarded = ['*'];
    protected $fillable = ['phone_number', 'is_primary'];

    public $belongsTo = [
        'user' => ['Cutesy\User\Models\User', 'key' => 'user_id']
    ];

    public function scopeIsPrimary($query)
    {
        return $query->where('is_primary', true);
    }

    public static function findById($id)
    {
        if (!$id) return;
        return self::where('id', $id)->first();
    }

    public static function findByUserId($user_id)
    {
        if (!$user_id) return;
        return self::where('user_id', $user_id)->first();
    }
}