<?php namespace Cutesy\User\Models;

use Mail;
use Event;
use Model;
use October\Rain\Auth\Models\User as OctoberUser;

/**
 * Model
 */
class User extends OctoberUser
{
    use \October\Rain\Database\Traits\SoftDelete;
    use \October\Rain\Database\Traits\Sluggable;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'cutesy_user_accounts';

    /**
     * Validation rules
     */
    public $rules = [];

    /**
     * @var array Relations
     */
    public $hasOne = [
        'profile'           => ['Cutesy\Community\Models\Profile'],
        'location'          => ['Cutesy\Location\Models\Location'],
        'invite'            => ['Cutesy\Community\Models\Invite', 'key' => 'invitee_id'],
        'request'           => ['Cutesy\Community\Models\Request'],
        'primary_phone'     => ['Cutesy\User\Models\Phone', 'conditions' => 'is_primary = true'],
    ];
    public $hasMany = [
        'emails'            => ['Cutesy\User\Models\Email'],
        'phones'            => ['Cutesy\User\Models\Phone'],
        'oauth_ids'         => ['Cutesy\User\Models\OauthId'],
    ];
    public $morphMany = [
        'invites'           => ['Cutesy\Community\Models\Invite', 'name' => 'inviter']
    ];

    /**
     * @var array The attributes that are mass assignable.
     */
    protected $fillable = ['name', 'surname', 'email', 'password'];

    /**
     * Purge attributes from data set.
     */
    protected $purgeable = ['password_confirmation'];

    protected $slugs = ['alias' => ['name', 'surname']];

    public function beforeCreate()
    {
        $this->uid = uniqid();
    }

    public function afterDelete()
    {
        if ($this->isSoftDelete()) {
            Event::fire('cutesy.user.deactivate', [$this]);
            return;
        }

        $this->avatar && $this->avatar->delete();

        parent::afterDelete();
    }

    public function afterLogin()
    {
        if ($this->trashed()) {
            $this->last_login = $this->freshTimestamp();
            $this->restore();

            Mail::sendTo($this->email, 'cutesy.user::mail.reactivate', [
                'name' => $this->name
            ]);

            Event::fire('cutesy.user.reactivate', [$this]);
        }
        else {
            parent::afterLogin();
        }

        Event::fire('cutesy.user.login', [$this]);
    }

    public static function findByEmail($email)
    {
        return self::where('email', $email)->first();
    }

    public function isBeauty()
    {
        if (!$profile = $this->profile)
            return false;

        if ($profile->role == $profile::ROLE_BEAUTY)
            return true;
    }

    public function isPro()
    {
        if (!$profile = $this->profile)
            return false;

        if ($profile->role == $profile::ROLE_PRO)
            return true;
    }

    public function hasUsedInvite()
    {
        if ($this->invite()->where('is_used', true)->first())
            return true;
    }

    public function hasSentRequest()
    {
        if ($this->request()->first())
            return true;
    }

    public function hasApprovedRequest()
    {
        if ($this->request()->where('is_approved', true)->first())
            return true;
    }

    public function scopeIsActivated($query)
    {
        return $query->where('is_activated', true);
    }

    public function attemptActivation($code)
    {
        $result = parent::attemptActivation($code);

        if ($result === false) {
            return false;
        }

        $mailTemplate = 'cutesy.user::mail.welcome';

        Mail::sendTo($this->email, $mailTemplate, [
            'name'  => $this->name,
            'email' => $this->email
        ]);

        return true;
    }

    public function getPersistCode()
    {
        if (!$this->persist_code) {
            return parent::getPersistCode();
        }

        return $this->persist_code;
    }

    public function getAvatarThumb($size = 25, $options = null)
    {
        if (is_string($options)) {
            $options = ['default' => $options];
        }
        elseif (!is_array($options)) {
            $options = [];
        }

        // Default is "mm" (Mystery man)
        $default = array_get($options, 'default', 'mm');

        if ($this->avatar) {
            return $this->avatar->getThumb($size, $size, $options);
        }
        else {
            return '//www.gravatar.com/avatar/'.
                md5(strtolower(trim($this->email))).
                '?s='.$size.
                '&d='.urlencode($default);
        }
    }

    //
    // Last Seen
    //

    /**
     * Checks if the user has been seen in the last 5 minutes, and if not,
     * updates the last_login timestamp to reflect their online status.
     * @return void
     */
    public function touchLastSeen()
    {
        if ($this->isOnline()) {
            return;
        }

        $oldTimestamps = $this->timestamps;
        $this->timestamps = false;

        $this
            ->newQuery()
            ->where('id', $this->id)
            ->update(['last_login' => $this->freshTimestamp()])
        ;

        $this->timestamps = $oldTimestamps;
    }

    /**
     * Returns true if the user has been active within the last 5 minutes.
     * @return bool
     */
    public function isOnline()
    {
        return $this->getLastSeen() > $this->freshTimestamp()->subMinutes(5);
    }

    /**
     * Returns the date this user was last seen.
     * @return Carbon\Carbon
     */
    public function getLastSeen()
    {
        return $this->last_login ?: $this->created_at;
    }
}