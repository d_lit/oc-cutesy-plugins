<?php namespace Cutesy\User\Models;

use Model;

/**
 * Email Model
 */
class Email extends Model
{
    use \October\Rain\Database\Traits\SoftDelete;

    public $table = 'cutesy_user_emails';
    public $rules = [
        'email_adress' => 'required|between:6,255|email|unique:cutesy_user_emails',
    ];

    protected $guarded = ['*'];
    protected $fillable = ['email', 'is_primary'];

    public $hasOne = [
        'invite'    => ['Cutesy\Community\Models\Invite'],
    ];
    public $hasMany = [
        'oauth_ids' => ['Cutesy\User\Models\OauthId'],
    ];
    public $belongsTo = [
        'user'      => ['Cutesy\User\Models\User'],
    ];

    public function scopeIsPrimary($query)
    {
        return $query->where('is_primary', true);
    }

    public static function findById($id)
    {
        if (!$id) return;
        return self::where('id', $id)->first();
    }

    public static function findByAdress($email)
    {
        if (!$email) return;
        return self::where('email', $email)->first();
    }

    public static function findByUserId($user_id)
    {
        if (!$user_id) return;
        return self::where('user_id', $user_id)->first();
    }

    public function hasValidInvite()
    {
        if ($this->invite()->where('is_used', false)->first())
            return true;
    }
}