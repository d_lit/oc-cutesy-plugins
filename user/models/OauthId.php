<?php namespace Cutesy\User\Models;

use Model;

/**
 * OauthId Model
 */
class OauthId extends Model
{
    public $table = 'cutesy_user_oauth_ids';

    protected $guarded = ['*'];
    protected $fillable = [
        'provider',
        'provider_user_id'
    ];

    public $belongsTo = [
        'email' => ['Cutesy\User\Models\Email'],
        'user'  => ['Cutesy\User\Models\User'],
    ];

}