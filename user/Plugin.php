<?php namespace Cutesy\User;

use App;
use Event;
use Backend;
use System\Classes\PluginBase;
use Illuminate\Foundation\AliasLoader;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Cutesy\User\Components\Session' => 'sessionComponent',
            'Cutesy\User\Components\Account' => 'accountComponent',
        ];
    }

    public function registerSettings()
    {
    }

    public function register()
    {
        $alias = AliasLoader::getInstance();

        $alias->alias('Auth', 'Cutesy\User\Facades\Auth');

        App::singleton('cutesy.auth', function() {
            return \Cutesy\User\Classes\AuthManager::instance();
        });
    }

    public function boot()
    {
        App::singleton('UserEventHandler', function() {
            return new \Cutesy\User\Classes\UserEventHandler;
        });

        Event::subscribe('UserEventHandler');
    }
}
