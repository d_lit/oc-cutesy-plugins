<?php namespace Cutesy\User\Components;

use Auth;
use Request;
use Redirect;
use Cms\Classes\ComponentBase;

class Session extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Session Component',
            'description' => ''
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    /**
     * Executed when this component is bound to a page or layout.
     */
    public function onRun()
    {
        //
    }

    /**
     * Log out the user
     *
     * Usage:
     *   <a data-request="onLogout">Sign out</a>
     *
     * With the optional redirect parameter:
     *   <a data-request="onLogout" data-request-data="redirect: '/good-bye'">Sign out</a>
     *
     */
    public function onLogout()
    {
        Auth::logout();
        $url = post('redirect', Request::fullUrl());

        return Redirect::to($url);
    }

    /**
     * Returns the logged in user, if available, and touches
     * the last seen timestamp.
     * @return Cutesy\User\Models\User
     */
    public function user()
    {
        if (!$user = Auth::getUser()) {
            return null;
        }

        $user->touchLastSeen();

        return $user;
    }
}