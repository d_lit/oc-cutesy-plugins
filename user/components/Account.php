<?php namespace Cutesy\User\Components;

use Lang;
use Auth;
use Mail;
use Event;
use Flash;
use Input;
use Request;
use Redirect;
use Validator;
use Exception;
use ApplicationException;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;

class Account extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'cutesy.user::lang.components.account.name',
            'description' => 'cutesy.user::lang.components.account.description'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getRedirectOptions()
    {
        return [''=>'- none -'] + Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function onRun()
    {
        $routeParameter = $this->param('code');
        /*
         * Activation code supplied
         */
        if ($activationCode = $this->param($routeParameter)) {
            $this->onActivate($activationCode);
        }
    }

    public function user()
    {
        if (!Auth::check()) {
            return null;
        }

        return Auth::getUser();
    }

    public function onRegister()
    {
        try {
            $data = post();
            $role = array_get($data, 'role');

            if (\Cutesy\User\Models\User::findByEmail(array_get($data, 'email'))) {
                throw new ApplicationException('
                    <i class="uk-icon uk-icon-envelope"></i>&nbsp;&nbsp;
                    Выбранный вами адрес уже занят
                ');
            }

            $credentials = [
                'name'      => array_get($data, 'name'),
                'surname'   => array_get($data, 'surname'),
                'email'     => array_get($data, 'email'),
                'password'  => array_get($data, "password_{$role}"),
                //'password_confirmation' => array_get($data, "password_{$role}_confirmation")
            ];

            $user = Auth::register($credentials, true);

            if (Event::fire('cutesy.user.register', [$this, $user, $data])) {
                //$this->sendActivationEmail($user);

                Auth::login($user);

                return [
                    'message' => '
                        <i class="uk-icon uk-icon-magic"></i>&nbsp;&nbsp;
                        Регистрация успешно завершена!
                    '
                ];
            }

        }
        catch (Exception $e) {
            throw new ApplicationException($e->getMessage());
        }
    }

    public function onSignin()
    {
        /*
         * Validate input
         */
        $credentials = post();

        /*
         * Authenticate user
         */
        $credentials = [
            'email'    => array_get($credentials, 'email'),
            'password' => array_get($credentials, 'password')
        ];

        Event::fire('cutesy.user.beforeAuthenticate', [$this, $credentials]);

        $user = Auth::authenticate($credentials, true);

        return [
            'message' => '<i class="uk-icon uk-icon-magic"></i>&nbsp;&nbsp;Вход успешно выполнен!'
        ];
    }

    public function onActivate($code = null)
    {
        try {
            $code = post('code', $code);

            /*
             * Break up the code parts
             */
            $parts = explode('!', $code);
            if (count($parts) != 2) {
                throw new ValidationException(['code' => Lang::get('cutesy.user::lang.components.account.invalid_activation_code')]);
            }

            list($userId, $code) = $parts;

            if (!strlen(trim($userId)) || !($user = Auth::findUserById($userId))) {
                throw new ApplicationException(Lang::get('cutesy.user::lang.components.account.invalid_user'));
            }

            if (!$user->attemptActivation($code)) {
                throw new ValidationException(['code' => Lang::get('cutesy.user::lang.components.account.invalid_activation_code')]);
            }

            Flash::success(Lang::get('cutesy.user::lang.components.account.success_activation'));

            /*
             * Sign in the user
             */
            Auth::login($user);

        }
        catch (Exception $ex) {
            if (Request::ajax()) throw $ex->getMessage();
        }
    }

    public function onAttemptInvitation($code)
    {
        $parts = explode('!', $code);

        if (count($parts) != 2) {
            return redirect('/');
        }

        list($id, $code) = $parts;

        $invite = InviteModel::find($id);

        if (!strlen(trim($id)) || !$invite) {
            return redirect('/');
        }

        if (!$invite->attemptInvitationCode($code)) {
            return redirect('/');
        }

        $user = $this->user();

        if(!$user) {
            $user = UserModel::firstOrCreate(['email' => $invite->email]);
        }

        Auth::login($user, true);

        if (Event::fire('cutesy.invite.apply', [$this, $user, $invite])) {
            return redirect('/#!create/pro');
        }
    }

    public function onUpdate()
    {
        if (!$user = $this->user()) {
            return;
        }

        $user->fill(post());
        $user->save();

        /*
         * Password has changed, reauthenticate the user
         */
        if (strlen(post('password'))) {
            Auth::login($user->reload(), true);
        }

        Flash::success(post('flash', Lang::get('cutesy.user::lang.components.account.success_saved')));

        /*
         * Redirect
         */
        if ($redirect = $this->makeRedirection()) {
            return $redirect;
        }
    }

    public function onDeactivate()
    {
        if (!$user = $this->user()) {
            return;
        }

        if (!$user->checkHashValue('password', post('password'))) {
            throw new ValidationException(['password' => Lang::get('cutesy.user::lang.components.account.invalid_deactivation_pass')]);
        }

        $user->delete();
        Auth::logout();

        Flash::success(post('flash', Lang::get('cutesy.user::lang.components.account.success_deactivation')));

        /*
         * Redirect
         */
        if ($redirect = $this->makeRedirection()) {
            return $redirect;
        }
    }

    public function onSendActivationEmail()
    {
        try {
            if (!$user = $this->user()) {
                throw new ApplicationException(Lang::get('cutesy.user::lang.components.account.login_first'));
            }

            if ($user->is_activated) {
                throw new ApplicationException(Lang::get('cutesy.user::lang.components.account.already_active'));
            }

            Flash::success(Lang::get('cutesy.user::lang.components.account.activation_email_sent'));

            $this->sendActivationEmail($user);

        }
        catch (Exception $ex) {
            if (Request::ajax()) throw $ex;
            else Flash::error($ex->getMessage());
        }

        /*
         * Redirect
         */
        if ($redirect = $this->makeRedirection()) {
            return $redirect;
        }
    }

    protected function sendActivationEmail($user)
    {
        $code = implode('!', [$user->id, $user->getActivationCode()]);
        $link = $this->currentPageUrl([
            $this->property('paramCode') => $code
        ]);

        $data = [
            'name' => $user->name,
            'link' => $link,
            'code' => $code
        ];

        Mail::send('cutesy.user::mail.activate', $data, function($message) use ($user) {
            $message->to($user->email, $user->name);
        });
    }

    protected function makeRedirection()
    {
        $redirectUrl = $this->pageUrl($this->property('redirect'))
            ?: $this->property('redirect');

        if ($redirectUrl = post('redirect', $redirectUrl)) {
            return Redirect::to($redirectUrl);
        }
    }
}