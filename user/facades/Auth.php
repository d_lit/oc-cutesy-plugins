<?php namespace Cutesy\User\Facades;

use October\Rain\Support\Facade;

class Auth extends Facade
{
    /**
     * Get the registered name of the component.
     * 
     * Resolves to:
     * - Backend\Classes\AuthManager
     * 
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'cutesy.auth';
    }
}
