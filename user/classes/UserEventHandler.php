<?php namespace Cutesy\User\Classes;

class UserEventHandler
{
    public function onProfileComplete ($component, $user, $profile, $data)
    {
        $user->update($data);

        if(array_key_exists('phone_number', $data) && !empty($data['phone_number'])) {
            $user->primary_phone()->create([
                'phone_number' => $data['phone_number'],
                'is_primary'   => true
            ]);
        }

        return true;
    }

    public function onProfileUpdate ($component, $user, $profile, $data)
    {
        $user->update($data);

        if(array_key_exists('phone_number', $data) && !empty($data['phone_number'])) {
            if ($user->primary_phone) {
                $user->primary_phone()->update([
                    'phone_number' => $data['phone_number']
                ]);
            }
            else {
                $user->primary_phone()->create([
                    'phone_number' => $data['phone_number'],
                    'is_primary'   => true
                ]);
            }
        }

        return true;
    }

    public function onRequestCreate ($component, $user, $profile, $data)
    {
        if ($user->update($data)) {
            return true;
        }
    }

    public function subscribe($events)
    {
        $events->listen('cutesy.profile.complete',  'UserEventHandler@onProfileComplete');
        $events->listen('cutesy.profile.update',    'UserEventHandler@onProfileUpdate');

        $events->listen('cutesy.request.create',    'UserEventHandler@onRequestCreate');
    }
}