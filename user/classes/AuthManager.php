<?php namespace Cutesy\User\Classes;

use October\Rain\Auth\AuthException;
use October\Rain\Auth\Manager as RainAuthManager;

class AuthManager extends RainAuthManager
{
    protected static $instance;

    protected $userModel = 'Cutesy\User\Models\User';

    //protected $groupModel = 'Cutesy\User\Models\Group';

    protected $throttleModel = 'Cutesy\User\Models\Throttle';

    protected $useThrottle = true;

    protected $requireActivation = false;

    protected $sessionKey = 'cutesy_auth';

    public function init()
    {
        parent::init();
    }

    /**
     * Registers a user by giving the required credentials
     * and an optional flag for whether to activate the user.
     *
     * @param array $credentials
     * @param bool $activate
     * @return Models\User
     */
    public function register(array $credentials, $activate = true)
    {
        $user = $this->createUserModel();
        $user->fill($credentials);
        $user->save();

        if ($activate) {
            //$user->attemptActivation($user->getActivationCode());
        }

        // Prevents revalidation of the password field
        // on subsequent saves to this model object
        // $user->password = null;

        return $this->user = $user;
    }
}