<?php namespace Cutesy\User\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderCreateCutesyUserUsers extends Migration
{
    public function up()
    {
        Schema::create('cutesy_user_accounts', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('uid')->index();
            $table->string('name')->nullable();
            $table->string('surname')->nullable();
            $table->string('alias')->unique();
            $table->string('avatar_url')->nullable();
            $table->string('email')->unique();
            $table->string('password')->nullable();
            $table->string('activation_code')->nullable()->index();
            $table->string('persist_code')->nullable();
            $table->string('reset_password_code')->nullable()->index();
            $table->string('login_code')->nullable()->index();
            $table->boolean('is_activated')->nullable();
            $table->timestamp('activated_at')->nullable();
            $table->timestamp('last_login')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('cutesy_user_accounts');
    }
}