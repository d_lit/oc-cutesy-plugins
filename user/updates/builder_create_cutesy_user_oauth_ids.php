<?php namespace Cutesy\User\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderCreateCutesyUserOauthIds extends Migration
{

    public function up()
    {
        Schema::create('cutesy_user_oauth_ids', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('email_id')->unsigned()->nullable();
            $table->string('provider_user_id');
            $table->string('provider');
            $table->string('access_token');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('cutesy_user_oauth_ids');
    }

}
