<?php namespace Cutesy\User\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderCreateCutesyUserEmails extends Migration
{

    public function up()
    {
        Schema::create('cutesy_user_emails', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('email')->unique();
            $table->boolean('is_primary')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('cutesy_user_emails');
    }

}
