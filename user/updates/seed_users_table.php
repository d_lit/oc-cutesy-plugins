<?php namespace Cutesy\User\Updates;

use Auth;
use Faker;
use Seeder;
use Cutesy\User\Models\User;
use Cutesy\User\Models\Email;
use Cutesy\User\Models\Phone;

class SeedUsersTable extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create('ru_RU');

        for($i = 1; $i <= 50; $i++)
        {
            $phone = Phone::create([
                'phone_number'  => '-79'.$faker->numerify('#########'),
                'is_primary'    => true,
            ]);

            $user = User::create([
                'name'                  => $faker->firstNameFemale(),
                'surname'               => "{$faker->lastName()}a",
                'email'                 => $faker->safeEmail(),
                'is_activated'          => true,
                'password'              => 'password',
                'password_confirmation' => 'password',
            ]);

            $user->phones()->save($phone);

            Auth::login($user);
        }
    }
}