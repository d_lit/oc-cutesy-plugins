<?php namespace Cutesy\User\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderCreateCutesyUserPhones extends Migration
{

    public function up()
    {
        Schema::create('cutesy_user_phones', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('phone_number')->unique();
            $table->boolean('is_primary')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('cutesy_user_phones');
    }

}
